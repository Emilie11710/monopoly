<?php

include_once ("Classes/Box.php");
include_once ("Classes/StreetBox.php");
include_once ("Classes/CardBox.php");
include_once ("Classes/TaxBox.php");
include_once ("Classes/PropertyBox.php");
include_once ("Classes/StationBox.php");

class BoardGame{
    public $boxes;

    public function __construct(){
        $this->boxes = [];
        $this->createBoardGame();
        echo "Création du plateau de jeu...<br><br>";
    }

    //création du plateau
    public function createBoardGame(){
        $box0 = new Box(0, "Case départ");
        $box1 = new StreetBox(1, "Boulevard de Belleville", 60, 2, 10, 30, 90, 160, 250, 50, 50, 30);
        $box2 = new CardBox(2, "Caisse de communauté", 1);
        $box3 = new StreetBox(3, "Rue Lecourbe", 60, 4, 20, 60, 180, 320, 450, 50, 50, 30);
        $box4 = new TaxBox(4, "Impôts sur le revenu", 200);
        $box5 = new StationBox(5, "Gare Montparnasse", 200);
        $box6 = new StreetBox(6, "Rue de Vaugirard", 100, 6, 30, 90, 270, 400, 550, 50, 50, 50);
        $box7 = new CardBox(7, "Chance", 2);
        $box8 = new StreetBox(8, "Rue de Courcelles", 100, 6, 30, 90, 270, 400, 550, 50, 50, 50);
        $box9 = new StreetBox(9, "Avenue de la République", 120, 8, 40, 100, 300, 450, 600, 50, 50, 60);
        $box10 = new Box(10, "Prison");
        $box11 = new StreetBox(11, "Boulevard de la Villette", 140, 10, 50, 150, 450, 625, 750, 100, 100, 70);
        $box12 = new PropertyBox(12, "Companie de distribution d'électricité", 150);
        $box13 = new StreetBox(13, "Avenue de Neuilly", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box14 = new StreetBox(14, "Rue de Paradis", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box15 = new StationBox(15, "Gare de Lyon", 200);
        $box16 = new StreetBox(16, "Avenue Mozart", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box17 = new CardBox(17, "Caisse de Communauté", 1);
        $box18 = new StreetBox(18, "Boulavard Saint Michel", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box19 = new StreetBox(19, "Place Pigale", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box20 = new Box(20, "Parc Gratuit");
        $box21 = new StreetBox(21, "Avenue Matignon", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box22 = new CardBox(22, "Chance", 2);
        $box23 = new StreetBox(23, "Boulevard Malesherbes", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box24 = new StreetBox(24, "Avenue Henri-Martin", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box25 = new StationBox(25, "Gare du Nord", 200);
        $box26 = new StreetBox(26, "Faubourg Saint-Honoré", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box27 = new StreetBox(27, "Place de la bourse", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box28 = new PropertyBox(28, "Companie de distribution des eaux", 150);
        $box29 = new StreetBox(29, "Rue Lafayette", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box30 = new Box(30, "Allez en prison");
        $box31 = new StreetBox(31, "Avenue de Breteuil", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box32 = new StreetBox(32, "Avenue Foch", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box33 = new CardBox(33, "Caisse de communauté", 1);
        $box34 = new StreetBox(34, "Boulevard des Capucines", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box35 = new StationBox(35, "Gare Saint-Lazare", 200);
        $box36 = new CardBox(36, "Chance", 2);
        $box37 = new StreetBox(37, "Avenue des Champs-Elysées", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        $box38 = new TaxBox(38, "Taxe de Luxe", 200);
        $box39 = new StreetBox(39, "Rue de la Paix", 60, 24, 120, 360, 850, 1025, 1200, 150, 150, 140);
        
        
        array_push($this->boxes, $box0, $box1, $box2, $box3, $box4, $box5, $box6, $box7, $box8, $box9, $box10, $box11, $box12, $box13, $box14, $box15, $box16, $box17, $box18, $box19, $box20, $box21, $box22, $box23, $box24, $box25, $box26, $box27, $box28, $box29, $box30, $box31, $box32, $box33, $box34, $box35, $box36, $box37, $box38, $box39);
            }

}