<?php
include_once ("Classes/Box.php");

class CardBox extends Box{
    public $cardValue;//1 pour carte"caisse de communauté", 2 pour carte "chance"

    public function __construct($value, $name, $cardValue){
        $this->cardValue = $cardValue;
        parent::__construct($value, $name);
    }

}