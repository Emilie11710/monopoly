<?php
include_once ("Classes/Card.php");

Class CommunityCard extends Card{
    public $communityCard;

    public function __construct($chanceOrCommunity){
        parent::__construct($chanceOrCommunity);
        $this->communityCard=[];
        $this->createCommunityCards();
        echo "Création du tas de cartes 'Caisse de communauté'.<br><br>";
    }

    //fonction qui génère le tas de cartes "Caisse de communauté"
    public function createCommunityCards(){
        $this->communityCard=[
            1=>"Recevez votre revenus annuel, 1500€.<br><br>",
            2=>"Vous êtes libéré de prison.<br><br>",
            3=>"Erreur de la banque en votre faveur, recevez 200€.<br><br>",
            4=>"Retournez à Belleville.<br><br>",
            5=>"La vente de votre stock vous rapporte 100€.<br><br>"
        ];
    }
}