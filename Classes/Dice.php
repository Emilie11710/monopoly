<?php


class Dice
{
    private $value;

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function __construct()
    {
        $this->value = $this->randomDice();
    }
    
    
    //défini un nombre aléatoire au dé entre 1 et 6
    public function  randomDice(){
        return rand(1,6);
    }

}