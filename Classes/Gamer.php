<?php
include_once ("Classes/Dice.php");
include_once ("Classes/Pawn.php");

class Gamer
{
    private $name;
    private $pawn;
    private $money;
    private $cards;
    private $stationNumber;

    
    public $alldiceValue;

    public function __construct($name, $pawnName)
{
    $this->name = $name;
    $this->pawn = new Pawn($pawnName);
    $this->money = 1500;
    $this->cards = 0;
    $this->stationNumber = 0;
    echo "Création d'un joueur...<br>";
    echo "Bonjour " . $this->name . ", vous avez choisi le pion " . $this->pawn->getName() . ".";
    echo "<br>";
    echo "Vous êtes sur la case Départ";
    echo "<br><br>";
}

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return object
     */
    public function getPawn()
    {
        return $this->pawn;
    }
    

    // Fonction qui permet au joueur de lancer 2 dés
    public function rollDice(){
        echo $this->name . " lance le dé ...";
        echo "<br>";

        $dice1 = new Dice();
        $dice2 = new Dice();

        echo "valeur du dé 1 : " . $dice1->getValue();
        echo "<br>";

        echo "valeur du dé 2 : " . $dice2->getValue();        
        echo "<br>"; 
        $this->allDiceValue = $dice1->getValue() + $dice2->getValue();
        echo $this->name . " avance de " . $this->allDiceValue . " cases." ;
    }

    //avancer sur le plateau
    public function moving(){

        $this->pawn->value += $this->allDiceValue;
        return $this->pawn->value;
    }

    //permet de savoir sur quelle case le pion se trouve
    public function whichBox($BoardGame, $Gamer, $LuckyCard, $CommunityCard, $gamers){
        $ValueGamer = $Gamer->moving();
        echo "<br>"; 
        foreach ( $BoardGame->boxes as $box){
            if ($box->value == $ValueGamer){
                echo $Gamer->getName() . " arrive sur la case " . $box->name . "<br>";
                $this->action($box, $LuckyCard, $CommunityCard, $Gamer, $gamers);
            }
        }
    }
    //défini la/les actions en fonction de la case
    public function action($box, $LuckyCard, $CommunityCard, $Gamer, $gamers){
        $class = get_Class($box);
        //si le joueur tombe sur sur case Carte
        if ($class == "CardBox"){
            $int = rand(1,5);
            if ($box->cardValue == 1){
                echo $CommunityCard->communityCard[$int];   
            }else{
                echo $LuckyCard->luckyCard[$int];
            }
        }
        //si le joueur tombe sur une carte propriété
        elseif ($class == "PropertyBox" || $class == "StreetBox" || $class == "StationBox"){
            //si pas de propriétaire
            if ($box->owner == false){
                if ($Gamer->money >= $box->price){
                    $box->owner = true;
                    $box->nameOwner = $Gamer->name;
                    $Gamer->money = $Gamer->money - $box->price;
                    echo "Vous devenez propriétaire de la carte, il vous reste ". $Gamer->money. " euros en poche.<br><br>";
                    if ($class == "StationBox"){
                        $Gamer->stationNumber ++;
                    }
                }else{
                    echo "Vous n'avez pas assez d'argent pour acheter cette propriété.<br><br>";
                }                
            //si case avec propriétaire
            }else{   
                //joueur pas proprio         
                if ($Gamer->name != $box->nameOwner){                
                    //case rue
                    if($class == "StreetBox"){
                        $priceToPay = 0;
                        switch($box->numberHouse){
                            case 0:
                                $priceToPay = $box->groundRent;
                                break;
                            case 1:
                                $priceToPay = $box->oneHouseRent;
                                break;
                            case 2:  
                                $priceToPay = $box->twoHouseRent;
                                break;
                            case 3:                                
                                $priceToPay = $box->treeHouseRent;
                                break;
                            case 4:                                
                                $priceToPay = $box->fourHouseRent;
                                break;
                            case 5:                                
                                $priceToPay = $box->hotelRent;
                                break;
                        }
                        $Gamer->money -= $priceToPay;
                        echo "Vous êtes sur la propriété de " .$box->nameOwner.". Vous devez payer ".$priceToPay." euros, il vous reste ". $Gamer->money. " euros en poche.<br>";
                        foreach ($gamers as $key=>$value){
                            if ($value->name == $box->nameOwner){
                                $value->money += $priceToPay;
                                echo $value->name ." reçoit ". $priceToPay . " euros. Il a maintenant ". $value->money . " en poche.<br><br>"; 
                            }
                        }
                        //case gare
                    }elseif($class == "StationBox"){
                        foreach ($gamers as $key=>$value){
                            if ($value->name == $box->nameOwner){
                                $numberOfStation = $value->stationNumber;
                                $priceToPay = 0;
                                switch($numberOfStation){
                                    case 0:
                                        break;
                                    case 1:
                                        $priceToPay = $box->oneStationRent;
                                        break;
                                    case 2:
                                        $priceToPay = $box->twoStationRent;
                                        break;
                                    case 3:
                                        $priceToPay = $box->treeStationRent;
                                        break;
                                    case 4:
                                        $priceToPay = $box->fourStationRent;
                                        break;
                                }
                                $Gamer->money -= $priceToPay;
                                echo "Vous êtes sur la propriété de " .$box->nameOwner.". Vous devez payer ".$priceToPay." euros, il vous reste ". $Gamer->money. " euros en poche.<br>";
                                $value->money += $priceToPay;
                                echo $value->name ." reçoit ". $priceToPay . " euros. Il a maintenant ". $value->money . " en poche.<br><br>"; 
                            }
                        }
                        //case compagnie eau ou electricité
                    }else{
                        $Gamer->money -= $Gamer ->alldiceValue * 4;
                        echo "Vous êtes sur la propriété de " .$box->nameOwner.". Vous devez payer ".($Gamer ->alldiceValue * 4)." euros, il vous reste ". $Gamer->money. " euros en poche.<br><br>";
                    }               
                   
                // joueur proprio
                }else{
                    echo "Vous êtes sur votre propriété.<br><br>";
                }
            }
        }elseif($class == "TaxBox"){
            $Gamer->money -= $box->cost;
            echo "Vous devez payer ".$box->cost. " euros. Il vous reste ".$Gamer->money." euros en poche.<br><br>";
        }else{
            echo "Il ne se passe rien. <br><br>";

        }
        //TODO faire les autres actions sur case
    
    }

}

