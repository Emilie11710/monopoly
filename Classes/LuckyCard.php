<?php
include_once ("Classes/Card.php");

Class LuckyCard extends Card{
    public $luckyCard;

    public function __construct($chanceOrCommunity){
        parent::__construct($chanceOrCommunity);
        $this->luckyCard =[];
        $this->createLuckyCards();
        echo "Création du tas de cartes 'Chance'.<br><br>";
    }
    
    //fonction qui génère le tas de cartes "chance"
    public function createLuckyCards(){
        $this->luckyCard=[
            1=>"Allez en Prison. Ne passez pas par la case départ, ne recevez pas 200€.<br><br>",
            2=>"Vous avez gagné le prix de mots croisés, recevez 50€.<br><br>",
            3=>"Vous aidez Gepeto dans sa menuiserie, recevez 50€.<br><br>",
            4=>"Reculez de 3 cases.<br><br>",
            5=>"Vous avez effacé les parties de vos amis, versez leur 50€ à chacun.<br><br>"
        ];
    }

    
}