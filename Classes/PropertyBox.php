<?php
include_once ("Classes/Box.php");

class PropertyBox extends Box{
    public $owner;
    public $nameOwner;
    public $price;


    public function __construct($value, $name, $price){
        $this->owner = false;
        $this->nameOwner = null;
        $this->price = $price;
        parent::__construct($value, $name);
    }

}