<?php
include_once ("Classes/PropertyBox.php");

class StationBox extends PropertyBox{
    public $oneStationRent;
    public $twoStationRent;
    public $treeStationRent;
    public $fourStationRent;

    
    public function __construct($value, $name, $price){
        $this->oneStationRent = 25;
        $this->twoStationRent = 50;
        $this->treeStationRent = 100;
        $this->fourStationRent = 200;
        parent::__construct($value, $name, $price);
    }
}

