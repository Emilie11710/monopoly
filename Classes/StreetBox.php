<?php
include_once ("Classes/PropertyBox.php");

class StreetBox extends PropertyBox{
    public $numberHouse;
    public $groundRent;
    public $oneHouseRent;
    public $twoHouseRent;
    public $treeHouseRent;
    public $fourHouseRent;
    public $hotelRent;
    public $allGround;
    public $houseCost;
    public $hotelCost;
    public $mortgageValue;

    public function __construct($value, $name, $price, $groundRent, $oneHouseRent, $twoHouseRent, $treeHouseRent, $fourHouseRent, $hotelRent, $houseCost, $hotelCost, $mortgageValue){
        $this->numberHouse = 0;
        $this->groundRent = $groundRent;
        $this->oneHouseRent = $oneHouseRent;
        $this->twoHouseRent = $twoHouseRent;
        $this->treeHouseRent = $treeHouseRent;
        $this->fourHouseRent = $fourHouseRent;
        $this->hotelRent = $hotelRent;
        $this->allGround = false;
        $this->houseCost = $houseCost;
        $this->hotelCost = $hotelCost;
        $this->mortgageValue = $mortgageValue;
        parent::__construct($value, $name, $price);
    }

}