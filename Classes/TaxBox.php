<?php

include_once ("Classes/Box.php");

class TaxBox extends Box{
    public $cost;

    public function __construct($value, $name, $cost){
        parent::__construct($value, $name);
        $this->cost = $cost;
    }
}