<?php


class pawn
{
    private $name;
    public $value;

    public function __construct($name)
    {
        $this->name = $name;
        $this->value = 0;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}