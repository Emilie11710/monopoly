<html>
  <head>
    <title>Monopoly</title>
  </head>
  <body>
  <h1>Monopoly</h1>
  

<?php
 include_once ("Classes/Gamer.php");
 include_once ("Classes/Pawn.php");
 include_once ("classes/BoardGame.php");
 include_once ("classes/Box.php");
 include_once ("Classes/LuckyCard.php");
 include_once ("Classes/CommunityCard.php");

//création du plateau
 $BoardGame = new BoardGame();

 //création des cartes
 $LuckyCards = new LuckyCard(2);
 $CommunityCards = new CommunityCard(1);

 //tableau de joueurs
 $gamers = array();
   
     

 
 $Gamer1 = new Gamer("Juliette", "Chapeau");
 array_push($gamers, $Gamer1);
 $Gamer2 = new Gamer("Malone","Chien");
 array_push($gamers, $Gamer2);
 $Gamer3 = new Gamer("Kevin", "Fer à repasser");
 array_push($gamers, $Gamer3);

 $Gamer1->rollDice();
 $Gamer1->whichBox($BoardGame, $Gamer1, $LuckyCards, $CommunityCards, $gamers);

 $Gamer2->rollDice();
 $Gamer2->whichBox($BoardGame, $Gamer2, $LuckyCards, $CommunityCards, $gamers);

 $Gamer3->rollDice();
 $Gamer3->whichBox($BoardGame, $Gamer3, $LuckyCards, $CommunityCards, $gamers);


?>

  </body>
</html>


